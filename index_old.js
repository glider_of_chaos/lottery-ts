const generateGameListButton = document.getElementById('generate_game_list');
const giveAwayButton = document.getElementById('show_results');
const debugOutputDiv = document.getElementById('debug_output');
const markdownSource = document.getElementById('markdown_source');
const markdownRender = document.getElementById('markdown_render');

class GiveAway {
	constructor(giveAway) {
    [this.maxUserTickets, this.maxGameTickets, this.seed, this.items, this.users] =
      [giveAway.maxUserTickets, giveAway.maxGameTickets, giveAway.seed, giveAway.items, giveAway.users];
    this.users.forEach(user => {
      let totalTicketCount = 0;
      let maxTicketsForGame = 0;
      Object.entries(user.distribution).forEach(([game, tickets]) => {
        totalTicketCount += tickets;
        maxTicketsForGame = (tickets > maxTicketsForGame) ? tickets : maxTicketsForGame;
      });
      user.ticketsUsed = totalTicketCount;
      user.maxTickets = maxTicketsForGame;
    });
    this.items.forEach( item => {
      item.ticketBowl = [];
    });
    this.disqualifiedUsers = [];
    this.allocateTickets();
	}
  
  generateGameList() {
    let markdown = '---' +
                   '\n' +
                   '### Game List ###' +
                   '\n' +
                   '|:gift:|Game|Steam Link|' +
                   '\n' +
                   '|:--:|--|:--:|';
    this.items.forEach(game => {
      markdown += '\n' +
                  `|${game.letter.toUpperCase()}|${game.name}|[Link](${game.link})|`;
    });
    return markdown;
  }
  
  listDisqualificationReasons(user) {
    let reasons = '';
  
    if (user.songProvided === false) {
      reasons += 'Song is not provided';
    }
    if (user.requirementsMet === false) {
      reasons += '</br>' +
                 'Timeline or badges requirements not met';
    }
    if (user.ticketsUsed > this.maxUserTickets) {
      reasons += '</br>' +
                 `Total maximum tickets amount exceeded ( ${user.ticketsUsed} / ${this.maxUserTickets} )`;
    }
    if (user.maxTickets > this.maxGameTickets) {
      reasons += '</br>' +
                 `Maximum tickets amount for one game exceeded ( ${user.maxTickets} / ${this.maxGameTickets} )`;
    }
    
    return reasons.replace(/^<\/br>/, '');
  }
  
  allocateTickets() {
    let totalValidTickets = 0;
    this.users.forEach(user => {
      const disqualificationReasons = this.listDisqualificationReasons(user);
      if (disqualificationReasons.length) {
        this.disqualifiedUsers.push({
          user: user.name,
          reason: disqualificationReasons
        });
      }
      else {
        Object.entries(user.distribution).forEach(([game, tickets]) => {
          let desiredGame = this.items.find(item => item.letter === game);
          for (let i = 0; i < tickets; i++) {
            desiredGame.ticketBowl.push(user.name);
          }
        });
        totalValidTickets += user.ticketsUsed;
      }
    });
    this.totalValidTickets = totalValidTickets;
  }
  
  drawWinners() {
    Math.seedrandom(this.seed);
    this.items.forEach(item => {
      if (item.ticketBowl.length) {
        let winningNum = Math.floor(Math.random() * item.ticketBowl.length);
        item.winner = item.ticketBowl[winningNum];
      }
    });
  }
  
  getWinnersMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Winners ###' +
                   '\n' +
                   '|:gift:|Winner|Chance|' +
                   '\n' +
                   '|--|--|--:|';
    this.items.forEach(item => {
      if (item.ticketBowl.length) {
        const ticketsFromWinner = item.ticketBowl.filter(ticketOwner => ticketOwner === item.winner).length;
        const prettyChance = (100 * ticketsFromWinner / item.ticketBowl.length).toFixed(2);
        markdown += '\n';
        markdown += `|${item.name}|${item.winner}|${prettyChance}%|`;
      }
      else {
        markdown += '\n';
        markdown += `|${item.name}| - | - |`;
      }
    });
    return markdown;
  }
  
  getPopularityStatsMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Popularity ###' +
                   '\n' +
                   `There were ${this.users.length} entries with total number of ${this.totalValidTickets} valid tickets.` +
                   '\n' +
                   '|:gift:|:ticket:|Popularity|' +
                   '\n' +
                   '|--|--|--:|';
    
    this.items.forEach(item => {
      const gameTickets = item.ticketBowl.length;
      const prettyPercentage = ( 100 * gameTickets / this.totalValidTickets).toFixed(2);
      markdown += '\n' +
                  `|${item.name}|${gameTickets}|${prettyPercentage}|`;
    });
    
    return markdown;
  }
  
  getDisqualificationMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Disqualifications ###' +
                   '\n';
    
    if (this.disqualifiedUsers.length) {
      markdown += '|:(|Reason|' +
                  '\n' +
                  '|--|--|';
      this.disqualifiedUsers.forEach(user => {
        markdown += '\n' +
                    `|${user.user}| ${user.reason}|`;
      });
    }
    else {
      markdown += 'None! :blush:';
    }
    return markdown;
  }
}

function showMarkdown(markdown) {
  let converter = new showdown.Converter({tables: true});
  
  markdownSource.innerHTML = markdown.replace(/<\/br>/g, `&lt/br&gt`).replace(/\n/g, `</br>`);
  markdownRender.innerHTML = converter.makeHtml(markdown);
}

giveAwayButton.addEventListener('click', event => {
  giveAway.drawWinners();
  let markdown = giveAway.getWinnersMarkdown() +
                 '\n\n' +
                 giveAway.getPopularityStatsMarkdown() +
                 '\n\n' +
                 giveAway.getDisqualificationMarkdown();
  //debugOutputDiv.innerHTML = JSON.stringify(giveAway, null, 2);
  showMarkdown(markdown);
  //console.log(JSON.stringify(giveAway, null, 2));
  
})

generateGameListButton.addEventListener('click', event => {
  showMarkdown(giveAway.generateGameList());
})

let giveAway = new GiveAway({
  maxUserTickets: 20,
  maxGameTickets: 12,
  seed: 'random seed',
  items: [{
    letter: 'a',
    name: 'ENSLAVED: Odyssey to the West Premium Edition',
    link: 'https://store.steampowered.com/app/245280/ENSLAVED_Odyssey_to_the_West_Premium_Edition/'
  }, {
    letter: 'b',
    name: 'Pac-Man 256',
    link: 'https://store.steampowered.com/app/455400/PACMAN_256/'
  }, {
    letter: 'c',
    name: 'GoNNER',
    link: 'https://store.steampowered.com/app/437570/GoNNER/'
  }, {
    letter: 'd',
    name: 'Tekken',
    link: 'https://store.steampowered.com/app/389730/TEKKEN_7/'
  }, {
    letter: 'e',
    name: 'Scribblenauts Unlimited',
    link: 'https://store.steampowered.com/app/218680/Scribblenauts_Unlimited/'
  }, {
    letter: 'f',
    name: 'Dust: An Elysian Tail',
    link: 'https://store.steampowered.com/app/236090/Dust_An_Elysian_Tail/'
  }, {
    letter: 'g',
    name: 'One Way Heroics',
    link: 'https://store.steampowered.com/app/266210/one_way_heroics/'
  }, {
    letter: 'h',
    name: 'Mercenary Kings',
    link: 'https://store.steampowered.com/app/218820/Mercenary_Kings_Reloaded_Edition/'
  }, {
    letter: 'j',
    name: 'Abyss Odyssey',
    link: 'https://store.steampowered.com/app/255070/Abyss_Odyssey/'
  }, {
    letter: 'k',
    name: 'Vanguard Princess + Lilith Pack + Hilda Rize Pack',
    link: 'https://store.steampowered.com/app/262150/Vanguard_Princess/'
  }, {
    letter: 'l',
    name: 'REVOLVER 360 RE:ACTOR',
    link: 'https://store.steampowered.com/app/313400/REVOLVER360_REACTOR/'
  }],
  users:[{
    name:'Timmy',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      b: 5,
      c: 5,
      d: 5}
  },{
    name:'Billy',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      b: 12,
      f: 3}
  },{
    name:'Spike',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 1,
      c: 9,
      d: 5}
  },{
    name:'Dolf',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 12,
      c: 9,
      d: 5}
  },{
    name:'Tony',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 2,
      b: 1,
      c: 16,
      d: 1}
  },{
    name:'Larry',
    songProvided: true,
    requirementsMet: false,
    distribution: {
      a: 2,
      b: 2,
      c: 6,
      d: 5}
  },{
    name:'Alfred',
    songProvided: false,
    requirementsMet: true,
    distribution: {
      a: 1,
      c: 8,
      d: 6}
  }]});